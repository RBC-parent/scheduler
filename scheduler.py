# Datastore holds all tasks/jobs - each with info about where/how they run, their schedule, and when they were last run
# Those jobs are created/managed through the web gui
# When scheduler starts up, all jobs are read from DB and loaded into scheduler
# When a job is triggered, looks up the relevant job_id in the DBself.
# It sends task info to relevant nodule, updates DB row with last_triggered datetime, and triggers a log event
# Or periodically go through the DB and include new tasks

from models import Job
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime
import paho.mqtt.client as mqtt
from json import dumps
from uuid import uuid4
from os import environ
from time import sleep

# TODO add MQTT listener to add_jobs topic, callback for add_job
# TODO listen for completed tasks and update last_triggered
presence_topic = 'presence/scheduler'
scheduler_topic = 'scheduler/'
job_topic = 'jobs/{nodule_id}'

broker = environ.get('broker', "mqtt")
client = mqtt.Client('scheduler')


def gen_task_id():
    """Generate a uid to identify components/jobs which is short enough to be human-usable"""
    uid = str(uuid4()).split('-')[0]
    return uid


def get_all_tasks():
    tasks = Job.select()
    return tasks
    pass


def task_creator(task_id):
    def call_task():
        # Get task fresh from DB
        task = Job.get(uid=task_id)
        print(str(datetime.now()), task.id)
        client.publish(job_topic.format(nodule_id=task.nodule.uid),
                       dumps({"task_id": task.id,
                              "run_id": gen_task_id(),
                              "pin": task.component.pin,
                              "kind": task.component.component_type,
                              "action": "read",  # TODO get action from DB?
                              "args": task.params}))
        task.save()
        # print(task.uid, task.name)
    return call_task


def load_in_all_tasks(scheduler):
    tasks = get_all_tasks()
    for task in tasks:
        task_func = task_creator(task.uid)
        scheduler.add_job(task_func, 'cron', id=task.uid, **task.cron)


def make_task_loader_task(scheduler):
    def load_in_new_tasks():
        # print("Checking DB for new/obsolete tasks")
        add_tasks, remove_tasks = find_task_diffs()
        for task_id in add_tasks:
            task = Job.get(uid=task_id)
            print("adding ", task.name)
            task_func = task_creator(task)
            scheduler.add_job(task_func, 'cron', id=task.uid, **task.cron)
        for task_id in remove_tasks:
            scheduler.remove_job(task_id)
        return

    def find_task_diffs():
        current_tasks = set([t.id for t in scheduler.get_jobs()])
        all_tasks = set([t.uid for t in get_all_tasks()])
        to_add = list(all_tasks - current_tasks)
        if len(to_add) > 0:
            print(to_add, " will be added")
        to_remove = list((current_tasks - all_tasks) - set(['task_refresher', 'update_RTC']))
        if len(to_remove) > 0:
            print(to_remove, " will be removed")
        return (to_add, to_remove)
    return load_in_new_tasks


def update_RTC():
    time_topic = "time"
    time = datetime.now()
    msg = dumps({"time": str(time)})
    # print(msg)
    client.publish(time_topic, msg)


def on_message(client, userdata, message):
    # Might use this to communicate with scheduler in realtime
    sched = userdata
    print(sched)
    # msg_json = loads(message.payload.decode("utf-8"))
    return


scheduler = BackgroundScheduler()
scheduler.start()
print(scheduler)
load_in_new_tasks = make_task_loader_task(scheduler)
scheduler.add_job(load_in_new_tasks, 'cron', second='*/5', id='task_refresher')
scheduler.add_job(update_RTC, 'cron', second='*/10', id='update_RTC')
load_in_all_tasks(scheduler)

client.user_data_set(scheduler)
client.on_message = on_message
print("Connecting to broker at " + broker)
client.connect(broker)
client.loop_start()
client.publish(presence_topic, '{"presence":"Scheduler connected"}')
client.subscribe(scheduler_topic)

run_forever = True
while run_forever:
    sleep(10)
    # TODO run forever until mqtt stop command
