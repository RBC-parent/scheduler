from peewee import TextField, ForeignKeyField, DateTimeField, FloatField, IntegerField, BooleanField, TimeField, Model
from playhouse.postgres_ext import PostgresqlDatabase, BinaryJSONField
from os import environ
from datetime import datetime
from uuid import uuid4
from time import sleep

host = environ.get("db_host", "0.0.0.0")
pw = environ.get("db_pw", "mysecretpassword")

sleep(5)
database = PostgresqlDatabase('postgres', **{'host': host, 'user': 'postgres', 'password': pw}, connect_timeout=60)


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = database


class Zone(BaseModel):
    description = TextField()
    name = TextField()
    parent = ForeignKeyField(column_name='parent', field='id', model='self', null=True)
    url = TextField()
    def __str__(self):
        return self.name
    class Meta:
        table_name = 'zone'
try:
    Zone.create_table()
except:
    raise


class Nodule(BaseModel):
    created_at = DateTimeField()
    err_log_size = IntegerField(null=True)
    hw_type = TextField(null=True)
    lat = FloatField(null=True)
    log_size = IntegerField(null=True)
    lon = FloatField(null=True)
    name = TextField()
    power = FloatField(null=True)
    presence = BooleanField(null=True)
    tags = TextField(null=True)
    topics = BinaryJSONField(null=True)
    uid = TextField(unique=True)
    woken_at = DateTimeField(null=True)
    zone = ForeignKeyField(column_name='zone', field='id', model=Zone)
    def __str__(self):
        return '%s - %s' % (self.name, self.uid)
    class Meta:
        table_name = 'nodule'
try:
    Nodule.create_table()
except:
    raise


class Component(BaseModel):
    component_type = TextField()
    description = TextField()
    kind = TextField()
    name = TextField()
    nodule = ForeignKeyField(column_name='nodule', field='id', model=Nodule)
    pin = TextField()
    uid = TextField(unique=True)
    def __str__(self):
        return '%s - %s' % (self.name, self.uid)
    class Meta:
        table_name = 'component'
try:
    Component.create_table()
except:
    raise


class Job(BaseModel):
    at_time = TimeField(null=True)
    component = ForeignKeyField(column_name='component', field='id', model=Component)
    description = TextField()
    # interval = IntegerField(null=True)
    cron = BinaryJSONField()
    kind = TextField()
    name = TextField()
    nodule = ForeignKeyField(column_name='nodule', field='id', model=Nodule)
    params = BinaryJSONField(null=True)
    period = IntegerField(null=True)
    tags = TextField(null=True)
    uid = TextField(unique=True)
    units = TextField(null=True)
    last_triggered = DateTimeField(null=True)
    def __str__(self):
        return '%s - %s' % (self.name, self.uid)
    class Meta:
        table_name = 'job'
try:
    Job.create_table()
except:
    raise


def uuid():
    """Generate a uid to identify components/jobs which is short enough to be human-usable"""
    uid = str(uuid4()).split('-')[0]
    return uid


def populate_test_data():
    print("#####################")
    print("Creating DB schema and populating with test data")
    print("#####################")

    #
    Z0 = Zone.create(name='root', url='/', description='root')
    Z1 = Zone.create(name='3_bayside_village', url='/3_bayside_village', description='3 Bayside Village', parent=Z0)
    Z2 = Zone.create(name='bedroom', url='/3_bayside_village/bedroom', description='Bedroom', parent=Z1)
    Z3 = Zone.create(name='living_room', url='/3_bayside_village/living_room', description='Living Room', parent=Z1)
    Z4 = Zone.create(name='balcony', url='/3_bayside_village/bedroom/balcony', description='Balcony', parent=Z2)
    Z5 = Zone.create(name='counter', url='/3_bayside_village/living_room/counter', description='Counter Top', parent=Z3)
    Z6 = Zone.create(name='window1', url='/3_bayside_village/living_room/window', description='Window by desk', parent=Z3)
    Z7 = Zone.create(name='SE window', url='/3_bayside_village/living_room/se_window', description='South East Window', parent=Z3)
    Z8 = Zone.create(name='SW window', url='/3_bayside_village/living_room/sw_window', description='South West Window', parent=Z3)
    #
    N2 = Nodule.create(uid='def456', name='living room', created_at=datetime.now(), zone=Z3, hw_type='raspi')
    N1 = Nodule.create(uid='abc123', name='balcony', created_at=datetime.now(), zone=Z4, hw_type='opi')
    N3 = Nodule.create(uid='ghi789', name='SE Window', created_at=datetime.now(), zone=Z7, hw_type='opi')
    N5 = Nodule.create(uid='jkl012', name='SW Window', created_at=datetime.now(), zone=Z8, hw_type='esp32')
    #
    c1 = Component.create(uid=uuid(), name='balc temp/hmdy', description='balcony temperature/humidity', kind='sensor', component_type='DHT_11', pin="1", nodule=N1)
    c2 = Component.create(uid=uuid(), name='tom_soil_temp', description='tomato soil temp', kind='sensor', component_type='ds18b20', pin="i2c_2", nodule=N1)
    c3 = Component.create(uid=uuid(), name='aub_soil_temp', description='aubergine soil temp', kind='sensor', component_type='ds18b20', pin="i2c_3", nodule=N1)
    c4 = Component.create(uid=uuid(), name='tom_soil_moist', description='tomato soil moisture', kind='sensor', component_type='moisture', pin="4", nodule=N1)
    c5 = Component.create(uid=uuid(), name='aub_soil_moist', description='aubergine soil moisture', kind='sensor', component_type='moisture', pin="5", nodule=N1)
    c6 = Component.create(uid=uuid(), name='balc lux', description='balcony light intensity', kind='sensor', component_type='TSL2561', pin="i2c_6", nodule=N1)
    c7 = Component.create(uid=uuid(), name='window', description='greenhouse window', kind='actuator', component_type='servo', pin="7", nodule=N1)
    c8 = Component.create(uid=uuid(), name='pump', description='irrigation pump', kind='actuator', component_type='pump', pin="8", nodule=N1)
    #
    #
    j1 = Job.create(uid=uuid(), name='balcony air temp', description='Balcony air temperature and humidity', kind='sensor', cron={'second': '5'}, units='C/%', tags='_', component=c1, nodule=N1, params={"foo": "bar"})
    j2 = Job.create(uid=uuid(), name='tomato soil temp', description='Temperature of soil in tomato pot', kind='sensor', cron={'second': '20'}, units='C', tags='_', component=c2, nodule=N1, params={"foo": "bar"})
    j3 = Job.create(uid=uuid(), name='aubergine soil temp', description='Temperature of soil in aubergine pot', kind='sensor', cron={'second': '20'}, units='C', tags='_', component=c3, nodule=N1, params={"foo": "bar"})
    j4 = Job.create(uid=uuid(), name='tomato soil moisture', description='Moisture of soil in tomato pot', kind='sensor', cron={'second': '20'}, units='%', tags='_', component=c4, nodule=N1, params={"foo": "bar"})
    j5 = Job.create(uid=uuid(), name='aubergine soil moisture', description='Moisture of soil in aubergine pot', kind='sensor', cron={'second': '20'}, units='%', tags='_', component=c5, nodule=N1, params={"foo": "bar"})
    j6 = Job.create(uid=uuid(), name='balcony light', description='Light intensity on balcony', kind='sensor', cron={'second': '5'}, units='lux', tags='_', component=c6, nodule=N1, params={"foo": "bar"})
    j6 = Job.create(uid=uuid(), name='run pump', description='Runs the pump', kind='actuator', cron={'second': '25'},  tags='_', component=c8, nodule=N1, params={"foo": "bar"})

    c9 = Component.create(uid=uuid(), name='chive_soil_moist', description='chive soil moisture', kind='sensor', component_type='moisture', pin="1", nodule=N2)
    c10 = Component.create(uid=uuid(), name='room lux', description='living room light intensity', kind='sensor', component_type='TSL2561', pin="i2c_2", nodule=N2)
    c11 = Component.create(uid=uuid(), name='lamp', description='thai lamp', kind='actuator', component_type='relay', pin="3", nodule=N2)
    c12 = Component.create(uid=uuid(), name='ds18b20', description='ds18b20', kind='sensor', component_type='ds18b20', pin="4", nodule=N1)

    j7 = Job.create(uid=uuid(), name='chive_moist', description='Moisture of soil in chive pot', kind='sensor', cron={'second': '17'},  tags='_', component=c9, nodule=N2, params={"foo": "bar"})
    j8 = Job.create(uid=uuid(), name='ds18b20', description='ds18b20', kind='sensor', cron={'second': '10'},  tags='_', component=c12, nodule=N1, params={"foo": "bar"})
    j8 = Job.create(uid=uuid(), name='room light', description='Light intensity in living rom', kind='sensor', cron={'second': '30'},  tags='_', component=c10, nodule=N2, params={"foo": "bar"})
    j9 = Job.create(uid=uuid(), name='room lamp', description='Turns on lamp in living room', kind='actuator', cron={'minute': '*/3'},  tags='_', component=c11, nodule=N2, params={"foo": "bar"})

    ipc = Component.create(uid=uuid(), name='ip_reporter', description='IP Reporter', kind='sensor', component_type='IP', pin="x", nodule=N1)
    ipj = Job.create(uid=uuid(), name='ip_reporter', description='IP Reporter', kind='sensor', cron={'second': '3'},  tags='_', component=ipc, nodule=N1, params={"foo": "bar"})
    ipc = Component.create(uid=uuid(), name='ip_reporter', description='IP Reporter', kind='sensor', component_type='IP', pin="x", nodule=N2)
    ipj = Job.create(uid=uuid(), name='ip_reporter', description='IP Reporter', kind='sensor', cron={'second': '3'},  tags='_', component=ipc, nodule=N2, params={"foo": "bar"})
    ipc = Component.create(uid=uuid(), name='ip_reporter', description='IP Reporter', kind='sensor', component_type='IP', pin="x", nodule=N3)
    ipj = Job.create(uid=uuid(), name='ip_reporter', description='IP Reporter', kind='sensor', cron={'second': '3'},  tags='_', component=ipc, nodule=N3, params={"foo": "bar"})

    upc = Component.create(uid=uuid(), name='uptimer', description='Uptime Reporter', kind='sensor', component_type='uptime', pin="x", nodule=N1)
    upj = Job.create(uid=uuid(), name='uptimer', description='Uptime Reporter', kind='sensor', cron={'second': '3'},  tags='_', component=upc, nodule=N1, params={"foo": "bar"})
    upc = Component.create(uid=uuid(), name='uptimer', description='Uptime Reporter', kind='sensor', component_type='uptime', pin="x", nodule=N2)
    upj = Job.create(uid=uuid(), name='uptimer', description='Uptime Reporter', kind='sensor', cron={'second': '3'},  tags='_', component=upc, nodule=N2, params={"foo": "bar"})
    upc = Component.create(uid=uuid(), name='uptimer', description='Uptime Reporter', kind='sensor', component_type='uptime', pin="x", nodule=N3)
    upj = Job.create(uid=uuid(), name='uptimer', description='Uptime Reporter', kind='sensor', cron={'second': '3'},  tags='_', component=upc, nodule=N3, params={"foo": "bar"})

print("Done")

if len(list(Nodule.select())) == 0:
    print("No data in DB, will populate with test data")
    populate_test_data()
else:
    print("DB already provisioned")


# for x in [Nodule, Job, Component]:
#     print(x)
#     for n in x.select():
#         print(n)
#         n.delete()
